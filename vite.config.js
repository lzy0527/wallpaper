import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
export default defineConfig({
  base: process.env.NODE_ENV === 'production' ? './' : '/',
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
  resolve: {
    alias: {
        '@': path.resolve(__dirname, './src')
    }
},
  server: {
    host: '0.0.0.0',

    proxy: {
      '/wapi': {
        // target: 'http://yhxweb.cn', 
        target: 'http://localhost:7101', 
        changeOrigin: true
      },
      '/yapi': {
        // target: 'http://162.14.111.100:7101', 
        // target: 'http://192.168.100.23:7101',
        target: 'http://yhxweb.cn', 
        changeOrigin: true
      }
    }
  }
})
