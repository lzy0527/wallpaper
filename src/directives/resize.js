
// 创建一个弱引用的map，用来存储元素和其对应回调函数
const map = new WeakMap()

// 创建一个ResizeObserver实例，用来监听元素的变化
const ob = new ResizeObserver(entries => {
  entries.forEach(entry => {
    const handle = map.get(entry.target)
    handle && handle({
      width: entry.borderBoxSize[0].inlineSize,
      height: entry.borderBoxSize[0].blockSize
    })
  })
})
export default {
  mounted(el, binding) {
    map.set(el, binding.value)
    ob.observe(el)
  },
  unbind(el) {
    ob.unobserve(el)
  }
}