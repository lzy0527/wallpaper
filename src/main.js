import { createApp } from 'vue'
import App from './App.vue'
import resize from '@/directives/resize.js'
import VueLazyload from 'vue-lazyload'
import './style.css';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import '@/hooks/forbiddenLog.hook'
import router from './router';


const app = createApp(App);
app.directive('resize', resize)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(VueLazyload, ElementPlus, {
  preLoad: 1.3,
  // error: errorimage,
  // loading: loadimage,
  attempt: 1
}).use(router).mount('#app')
