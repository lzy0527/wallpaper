import request from './index'

export const getWallpaperByPage = (data) => request({
  method: 'get',
  url: `/wapi/ComputerWallpaper/getInfoList`,
  params: data
})
export const getWallpaperList = (data) => request({
  method: 'get',
  url: `/wapi/get-wallhaven`,
  params: {
    page: data.page,
    size: data.size,
    id: data.dir
  }
})

/**
 * 获取登录二维码
 * @param {*} data 
 * @returns 
 */
export const getLoginQrCode = (data) => request({
  method: 'post',
  url: `/yapi/wechat/getLoginQrCode`,
  params: data
})

/**
 * 扫码关注公众号登录 检测登录状态
 * @param {*} data 
 * @returns 
 */
export const checkLogin = (data) => request({
  method: 'post',
  url: `/yapi/wechat/checkLogin`,
  data: data
})