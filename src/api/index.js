import axios from "axios";
// 创建 Axios 实例
const request = axios.create({
  baseURL: "",
  timeout: 1000 * 60,
});

// 创建响应拦截器
request.interceptors.response.use(response => {
  return response.data;
}, error => {
  return Promise.reject(error);
});
export default request;
