import { ref } from 'vue'

// 头部导航栏高度 会随 resize 函数发生适配改变 不然小屏幕上高度还是150就太高不好看
export const header_height_g = ref(150)

// 头部导航栏与内容区之间的距离
export const header_margin_bottom_g = ref(10)

// 当前屏幕宽度
export const screen_width = ref(1920)

// 左侧菜单是否显示
export const isShowMenu = ref(false);

// 登录弹窗是否显示
export const isShowLoginDlg = ref(false)

// 手机图片分类汇总
export const phoneImgList = [
    {
        dir: 'all',
        desc: '全部',
        img: 'http://cdn.lzyblog.cn/computer/all/photo-1437764328215-36358ba8e5e4'
    },
]

// 电脑图片分类汇总
export const computerImgList = [
    {
        dir: '',
        desc: '最新',
        img: 'https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/qz/qzm1or.jpg?w=400&h=200&fmt=webp'
    },
    {
        dir: '5',
        desc: '动漫',
        img: 'https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/zy/zyd25v.jpg?w=400&h=200&fmt=webp'
    },
    {
        dir: '2319',
        desc: '蜘蛛侠',
        img: 'https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/x8/x8kqg3.jpg?w=400&h=200&fmt=webp'
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/o5/o5jg27.jpg?w=400&h=200&fmt=webp",
        "dir": "1",
        "desc": "日漫"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/rr/rrpjr1.jpg?w=400&h=200&fmt=webp",
        "dir": "78174",
        "desc": "火影忍者"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/vq/vq27p5.jpg?w=400&h=200&fmt=webp",
        "dir": "376",
        "desc": "赛博朋克"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/qz/qzo7lr.jpg?w=400&h=200&fmt=webp",
        "dir": "14",
        "desc": "科幻"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/zy/zyvrxy.jpg?w=400&h=200&fmt=webp",
        "dir": "711",
        "desc": "风景"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/wallhaven/small/3l/3l66qd.jpg?w=400&h=200&fmt=webp",
        "dir": "65348",
        "desc": "4k"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/unsplash/photo-1696258361311-0b25ee8af854?ixid=M3w0MjI2NjN8MHwxfHRvcGljfHxobWVudlFoVW14TXx8fHx8Mnx8MTcwNDM1OTMyN3w&ixlib=rb-4.0.3&w=300&h=200&fmt=webp",
        "dir": "10086",
        "desc": "摄影"
    },
    {
        "img": "https://dogefs.s3.ladydaily.com/~/source/unsplash/photo-1675150277436-9c7348972c11?ixid=M3wyNjY4NDZ8MHwxfHRvcGljfHx4alBSNGhsa0JHQXx8fHx8Mnx8MTcwNDI0NjEyOXw&ixlib=rb-4.0.3&w=300&h=200&fmt=webp",
        "dir": "100861",
        "desc": "美食"
    },
    {
        "img": "https://files.codelife.cc/itab/defaultWallpaper/videos/68.jpg?x-oss-process=image/resize,limit_0,m_fill,w_400,h_200/quality,q_92/format,webp&.svg",
        "dir": "video",
        "desc": "动态壁纸"
    }
]

// 登录弹窗是否显示
export const loginDialogVisible = ref(false);

// openid
export const openid_g = ref(undefined);

// sex
export const sex_g = ref(undefined);

// 当前模式 电脑  手机
export const currentMode = ref('computer')

// 当前显示的分类

export const computer_currentClassify = ref('')
export const phone_currentClassify = ref('all')