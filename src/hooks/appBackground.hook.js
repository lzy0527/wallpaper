
import { ref, onMounted, onBeforeUnmount } from 'vue';
import * as Three from "three";
// 这里引入具体的风格，比如云风格的 就引入vanta/src/vanta.clouds.js
import BIRDS from "vanta/src/vanta.birds";
import Fog from "vanta/src/vanta.fog";
import WAVES from "vanta/src/vanta.waves";
import Clouds from "vanta/src/vanta.clouds";
import Clouds2 from "vanta/src/vanta.clouds2"; // 有问题
import Globe from "vanta/src/vanta.globe";
import NET from "vanta/src/vanta.net";
import Cells from "vanta/src/vanta.cells";
import Rings from "vanta/src/vanta.rings";
import Halo from "vanta/src/vanta.halo";

const vantaEffect = ref(null);
let styleArr = ['BIRDS', 'Fog', 'WAVES', 'Clouds', 'Clouds2', 'Globe', 'NET', 'Cells', 'Rings', 'Halo']






function initThree() {
    // 这里重新设置样式
    vantaEffect.value.setOptions({
        mouseControls: true,
        touchControls: true,
        gyroControls: false,
        minHeight: 200.00,
        minWidth: 200.00,
        skyColor: 0x91cde3,
        cloudColor: 0xc9c9d9,
        cloudShadowColor: 0x174b7d,
        sunColor: 0xe37f05,
        speed: 1.50
    })

}

function threeChange(vantaRef) {
    let val = styleArr[getRanomIndex()]
    switch (val) {
        case 'NET':
            vantaEffect.value = NET({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'BIRDS':
            vantaEffect.value = BIRDS({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Fog':
            vantaEffect.value = Fog({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'WAVES':
            vantaEffect.value = WAVES({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Clouds':
            vantaEffect.value = Clouds({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Clouds2':
            vantaEffect.value = Clouds2({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Globe':
            vantaEffect.value = Globe({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Cells':
            vantaEffect.value = Cells({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Rings':
            vantaEffect.value = Rings({
                el: vantaRef.value,
                THREE: Three
            })
            break;
        case 'Halo':
            vantaEffect.value = Halo({
                el: vantaRef.value,
                THREE: Three
            })
            break;

        default:
            break;
    }
    initThree()
}


function getRanomIndex() {
    return Math.floor(Math.random() * styleArr.length)
}

export {
    threeChange,
    vantaEffect
}



