import { gsap } from "gsap";

let time = null;

const ob = new IntersectionObserver((entries) => {
  for (const entry of entries) {
    if (entry.isIntersecting) {
      const img = entry.target;
      img.src = img.dataset.src;
      img.style.opacity = 1;
      ob.unobserve(img);
    }
  }
  // gsapPlay();
},
  {
    threshold: 0,
  });

// gsap动画
function gsapPlay() {
  if (time !== null) {
    clearTimeout(time);
  }
  time = setTimeout(() => {
    gsap.fromTo('#obimg', {
      scale: 0.8,
      y: -50,
      opacity: 0,
      stagger: 0.2,
    },
      {
        scale: 1,
        y: 0,
        opacity: 1,
        stagger: 0.2,
      });
    time = null;
  }, 500)
}

function lazyLoadImages() {
  const imgs = document.querySelectorAll("img[data-src]");
  imgs.forEach((img, i) => {
    ob.observe(img);
  });
}

// 初始加载时执行一次  
lazyLoadImages();

// 添加新图片时再次执行  
document.addEventListener('DOMNodeInserted', lazyLoadImages);
