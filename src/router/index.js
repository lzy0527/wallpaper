import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/computerWallpaper',
    component: () => import('@/views/Home.vue'),
    children: [
      {
        path: '/computerWallpaper',
        name:'ComputerWallpaper',
        component: () => import('@/views/wallpaper/computerWallpaper.vue')
      },
      {
        path: '/phoneWallpaper',
        name:'PhoneWallpaper',
        component: () => import('@/views/wallpaper/phoneWallpaper.vue')
      },
    ]
  },
  {
    path: '/wxDownload',
    name: 'WxDownload',
    component: () => import('@/views/WxDownload.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router